.. XiVO-doc Documentation master file.

********************************************************
XiVO Solutions |version| Documentation (Jabbah Edition)
********************************************************

.. warning:: This version is not yet released, our last LTS is Izar_.

.. important:: **What's new in this version ?**

   See :ref:`jabbah_release` page for the complete list of **New Features** and **Behavior Changes**.
  
   **Deprecations**   

     * End of Support for LTS Deneb (2019.12).


.. figure:: logo_xivo.png
 

XiVO_ solutions developed by Wisper_ group is a suite of PBX applications based on several free existing components including Asterisk_
and our own developments. This powerful and scalable solution offers a set of features for corporate telephony and call centers to power their business.

You may also have a look at our `development blog <http://xivo-solutions-blog.gitlab.io/>`_ for technical news about the solution

.. _Asterisk: http://www.asterisk.org/
.. _Wisper: https://wisper.io/
.. _XiVO: https://xivo.solutions/
.. _Izar: https://documentation.xivo.solutions/en/2022.05/

.. toctree::
   :maxdepth: 2

   introduction/introduction
   getting_started/getting_started
   installation/index
   administrator/index
   ipbx_configuration/administration
   contact_center/contact_center
   edge/index
   meetingrooms/index
   ivr/index
   usersguide/index
   Devices <https://documentation.xivo.solutions/projects/devices/en/latest/>
   api_sdk/api_sdk
   contributing/contributing
   releasenotes/index


Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
