************
Introduction
************

XiVO solutions is a suite of PBX applications developed by Wisper_ group, based on several free existing components
including Asterisk_ Play_  Akka_ and Scala_. It provides a solution for enterprises who wish use modern communication services
(IPBX, Unified Messaging, ...) api and application to businesses.

It gives especially access to outsourced statistics, real-time supervision screens,
third-party CTI integration and recording facilities.

.. _Wisper: https://www.wisper.io/
.. _Avencall: http://www.avencall.com/
.. _Play: http://www.playframework.com/
.. _Akka: http://akka.io/
.. _Scala: http://www.scala-lang.org/
.. _free software: http://www.gnu.org/philosophy/free-sw.html
.. _Asterisk: http://www.asterisk.org/

XiVO solutions is `free software`_. Most of its distinctive components, and XiVO solutions as a whole, are distributed
under the *GPLv3 license* and or  the *LGPLv3 license*..


XiVO solutions documentation is also available as a downloadable HTML, EPUB or PDF file.
See the `downloads page <https://readthedocs.org/projects/xivo-polaris/downloads/>`_
for a list of available files or use the menu on the lower right.

XiVO History
============

XiVO was created in 2005 by Sylvain Boily (Proformatique SARL).
The XiVO mark was owned by Avencall_ SAS after a merge between Proformatique SARL and Avencall_ SARL
in 2010.
Since 2020, Avencall_ has been acquired by Wisper_ group.

The XiVO core team now works for Wisper_ in Lyon (France) and Prague (Czech Republic)

* XiVO 1.2 was released  on February 3, 2012.
* XiVO 13.07 was the last version with Asterisk 1.8.21.0
* XiVO 13.08 includes Asterisk 11.3.0 in May 2013
* XiVO 13.25 is running under Wheezy in December 2013
* XiVO 14.02 is now called XiVO Five to celebrate the editor 5th year
* XiVO 15.13 runs Asterisk 13 in July 2015
* XiVO 15.20 is running under Jessie in January 2016
* XiVO 2016.02 starts the new versioning system including XiVO-CC and XiVO-UC modules in October 2016
* XiVO solutions 2016.04 includes new XiVO assistant, web edition, mobile edition and Desktop edition in December 2016
* XiVO solutions 2017.03 in April 2017 is the first Long Term Support version known as Five_.The main goal is to offer a stable version every 6 months even if the team is still doing small and agile iterations of 3 weeks.
* XiVO solutions 2017.11 in October 2017, second LTS release known as Polaris_
* XiVO solutions 2018.05 in April 2018, third LTS release known as Aldebaran_,
* XiVO solutions 2019.16 in October 2018, 4th LTS release known as Borealis_,
* XiVO solutions 2019.05 in April 2019, 5th LTS release known as Callisto_,
* XiVO solutions 2019.12 in October 2019, 6th LTS release known as Deneb_,
* XiVO solutions 2020.07 in April 2020, 7th LTS release known as Electra_ that set Five_ version as out of support,
* XiVO solutions 2020.18 in October 2020, 8th LTS release known as Freya_ that set Polaris_ version as out of support,
* XiVO solutions 2021.07 in April 2021, 9th LTS release known as Gaia_ running on Asterisk 18 that set Aldebaran_ version as out of support,
* XiVO solutions 2021.15 in October 2021, 10th LTS release known as Helios_ that set Borealis_ version as out of support,
* XiVO solutions 2022.05 in April 2022, 11th LTS release Known as Izar_ that set Callisto_ version as out of support,

Next release is on the way, will be called XiVO Jabbah and will be available in October 2022.

.. _Five: https://documentation.xivo.solutions/en/2017.03/
.. _Polaris: https://documentation.xivo.solutions/en/2017.11/
.. _Aldebaran: https://documentation.xivo.solutions/en/2018.05/
.. _Borealis: https://documentation.xivo.solutions/en/2018.16/
.. _Callisto: https://documentation.xivo.solutions/en/2019.05/
.. _Deneb: https://documentation.xivo.solutions/en/2019.12/
.. _Electra: https://documentation.xivo.solutions/en/2020.07/
.. _Freya: https://documentation.xivo.solutions/en/2020.18/
.. _Gaia: https://documentation.xivo.solutions/en/2021.07/
.. _Helios: https://documentation.xivo.solutions/en/2021.15/
.. _Izar: https://documentation.xivo.solutions/en/2022.05/

GDPR
====
XiVO architecture was redesigned to be GDPR compliant by being "privacy by design", which is the GDPR DNA.
We have in this respect refined all the features since Polaris to be 100% compliant.
