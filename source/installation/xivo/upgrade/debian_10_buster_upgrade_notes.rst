.. _debian_10_upgrade_notes:

********************************
Debian 10 (Buster) Upgrade Notes
********************************

Debian was upgraded to Debian 10 (Buster) in XiVO 2020.10 release.

.. warning:: Upgrade from versions *earlier* than XiVO Boréalis (2018.16) are not supported.
     You MUST first upgrade to at least XiVO Boréalis (2018.16) or more before upgrading to XiVO Freya.


.. _debian_10_upgrade_notes_before:

Before the upgrade
==================

.. important:: **Make sure you have sufficient space for the upgrade**.
   You should have more than 2GiB available in the filesystem that holds the ``/var`` and ``/`` directories.

* It is not possible to upgrade from XiVO below Boréalis (2018.16) version. You first need to upgrade to XiVO Boréalis.
* Note that the upgrade will take longer than usual because of all the system upgrade.
* You **MUST** deactivate all non-xivo apt sources list:

  * in directory `/etc/apt/sources.list.d/` you should only have the files :file:`xivo-dist.list` and (from Aldebaran) :file:`docker.list` :file:`pgdg.list`.
  * you **MUST** suffix all other files with `.save` to deactivate them.
* You **MUST** check the Debian sources list are correct: the file :file:`/etc/apt/sources.list` must contain the following and only the following::

    deb http://ftp.fr.debian.org/debian/ stretch main
    deb-src http://ftp.fr.debian.org/debian/ stretch main

    deb http://security.debian.org/ stretch/updates main
    deb-src http://security.debian.org/ stretch/updates main

    # stretch-updates, previously known as 'volatile'
    deb http://ftp.fr.debian.org/debian/ stretch-updates main
    deb-src http://ftp.fr.debian.org/debian/ stretch-updates main

* You may want to clean your system before upgrading:

  * Remove package that were automatically installed and are not needed anymore::

     apt-get autoremove --purge
  * Purge removed packages. You can see the list of packages in this state by running ``dpkg -l |
    awk '/^rc/ { print $2 }'`` and purge all of them with::

      apt-get purge $(dpkg -l | awk '/^rc/ { print $2 }')
  * Remove :file:`.dpkg-old`, :file:`.dpkg-dist` and :file:`.dpkg-new` files from previous upgrade. You can see a list
    of these files by running::

      find /etc -name '*.dpkg-old' -o -name '*.dpkg-dist' -o -name '*.dpkg-new'


.. _debian_10_upgrade_notes_after:

After the upgrade
=================

* After having check your network configuration and the grub configuration, you **MUST** *reboot your system*. It is necessary for the upgrade to the Linux kernel to be effective.
* Check that customization to your configuration files is still effective.

  During the upgrade, new version of configuration files are going to be installed, and these might
  override your local customization. For example, the vim package provides a new :file:`/etc/vim/vimrc`
  file. If you have customized this file, after the upgrade you'll have both a :file:`/etc/vim/vimrc` and
  :file:`/etc/vim/vimrc.dpkg-old` file, the former containing the new version of the file shipped by the vim
  package while the later is your customized version. You should merge back your customization into
  the new file, then delete the :file:`.dpkg-old` file.

  If collectd is installed, you will need to do the same with the :file:`/etc/collectd/collectd.conf` file.

  You can see a list of affected files by running ``find /etc -name '*.dpkg-old'``. If some files
  shows up that you didn't modify by yourself, you can ignore them.

* Purge removed packages. You can see the list of packages in this state by running ``dpkg -l |
  awk '/^rc/ { print $2 }'`` and purge all of them with::

    apt-get purge $(dpkg -l | awk '/^rc/ { print $2 }')


Changes
=======

No major change to advertise here that would come with XiVO on Debian 10.


External Links
==============

`Official Debian 10 Release Notes <https://www.debian.org/releases/buster/amd64/release-notes/>`_

