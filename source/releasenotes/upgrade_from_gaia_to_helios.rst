**********************
Upgrade Gaia to Helios
**********************

.. contents:: :local:

Before Upgrade
==============

On XiVO CC/UC
-------------

* Reporting: During upgrade all **Kibana configuration** (including the dashboard) will be lost (it is stored in *elasticsearch* container).
  You **MUST** :ref:`backup Kibana configuration <xivocc_backup_kibana>` before the upgrade.


After Upgrade
=============

On XiVO PBX
-----------

* A new certificate was intalled for XiVO Nginx in :file:`/etc/docker/nginx/ssl/`. This certificate will be used by the Webi. See :ref:`https_certificate` for more information explanation.

  .. warning:: If you had installed, **on the XiVO PBX**, a trusted certificate **you must** replace this new generated certificate by the one you installed previously.
       Normally this means that you should copy ``/usr/share/xivo-certs/server.{crt,key}`` files into ``/etc/docker/nginx/ssl/xivoxc.{crt,key}`` files::

         cp /usr/share/xivo-certs/server.crt /etc/docker/nginx/ssl/xivoxc.crt
         cp /usr/share/xivo-certs/server.key /etc/docker/nginx/ssl/xivoxc.key

       And then reload Nginx::

         xivo-dcomp reload nginx

       See :ref:`cert_change_xivonginx_cert` for generic explanation.


On XiVO CC/UC
-------------

* **UC-Addon**: A new certificate was installed for XiVO Nginx in :file:`/etc/docker/nginx/ssl/`. This certificate will be used by the XiVO Webi **and** the UC application.
  See :ref:`https_certificate` for more information explanation.

   .. warning:: You most probably had installed a trusted certificate to be able to use UC application. In this case **you must** replace this new generated certificate by the one you installed previously.
       Normally this means that you should copy ``/usr/share/xivo-certs/server.{crt,key}`` files into ``/etc/docker/nginx/ssl/xivoxc.{crt,key}`` files::

         cp /usr/share/xivo-certs/server.crt /etc/docker/nginx/ssl/xivoxc.crt
         cp /usr/share/xivo-certs/server.key /etc/docker/nginx/ssl/xivoxc.key

       And then reload Nginx::

         xivo-dcomp reload nginx

       See :ref:`cert_change_xivonginx_cert` for generic explanation.

* Reporting:

  * :ref:`Restore Kibana configuration <xivocc_restore_kibana>`.
  * The last 7 days of data will be re-replicated to Elasticsearch, see :ref:`totem_data_flow`.
    It may take some time if you have a huge amount of calls per week (more than 1 hour if you have 2 million of queue_log per week).

* **XDS installation only**: you need to update the nginx configuration for WebRTC on MDS if you had already followed the :ref:`xds_webrtc-configuration`

  #. On your XiVO CC hosting the nginx server, edit the file :file:`/etc/docker/nginx/sip_proxy/sip_proxy.conf`
     (if this file doesn't exist on your server you can skip this)
  #. Inside the location,

     #. remove all the lines containing the parameters:

        * ``proxy_http_version``,
        * ``proxy_set_header``,
        * ``proxy_{connect,read,send}_timeout``,
        * and ``proxy_buffering off``
     #. and replace it by the two following includes::

         include /etc/nginx/xivo/proxy-ws_params;
         include /etc/nginx/xivo/proxy_params;
     #. be sure to keep the ``keepalive_timeout 180s`` parameter.
  #. Below is an example before/after with mds1 as MDS_NAME:

   +------------------------------------------+--------------------------------------------+
   | Before                                   | After                                      |
   +==========================================+============================================+
   | .. code-block:: ini                      | .. code-block:: ini                        |
   |  :emphasize-lines: 5-12                  |  :emphasize-lines: 5-6                     |
   |                                          |                                            |
   |  location /wssip-mds1 {                  |  location /wssip-mds1 {                    |
   |   auth_request /validatetoken;           |   auth_request /validatetoken;             |
   |                                          |                                            |
   |   proxy_pass http://10.32.0.201:5039/ws; |   proxy_pass http://10.32.0.201:5039/ws;   |
   |   proxy_http_version 1.1;                |   include /etc/nginx/xivo/proxy-ws_params; |
   |   proxy_set_header Upgrade $http_upgrade;|   include /etc/nginx/xivo/proxy_params;    |
   |   proxy_set_header Connection "upgrade"; |                                            |
   |   proxy_set_header Host $host;           |                                            |
   |   proxy_buffering off;                   |                                            |
   |   proxy_connect_timeout 1m;              |                                            |
   |   proxy_read_timeout 5m;                 |                                            |
   |   proxy_send_timeout 5m;                 |                                            |
   |   keepalive_timeout 180s;                |   keepalive_timeout 180s;                  |
   |  }                                       |  }                                         |
   +------------------------------------------+--------------------------------------------+

* Access to :ref:`deprecated API <restapi_xuc_deprecated>`:
  
  * The DEPRECATED_API_HOST variable is now set to XIVO_HOST as default to allow XIVO_HOST to use agent related function keys.
  * If the variable was previously set in custom.env, the value will also be reseted to XIVO_HOST.
  * The value can be changed by setting the allowed IP addresses to the DEPRECATED_HOST_VARIABLE in the :file:`/etc/docker/compose/custom.env`.

On Edge
-------

If you already had a Edge server installed and configured in Gaia. Then you need to:

#. add and fill the ``XIVO_HOST`` var in the :file:`.env` file of the Edge Web Server
#. add and fill the ``TURN_ALLOWED_PEERS`` var in the :file:`.env` file of the Edge TURN Server - follow :ref:`edge_config_turnrelaypermission`

