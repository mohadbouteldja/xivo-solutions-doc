*********************************
XiVO Jabbah Intermediate Versions
*********************************

2022.07 (IV Jabbah)
===================

Consult the `2022.07 (IV Jabbah) Roadmap <https://projects.xivo.solutions/versions/303>`_.

Components updated: **edge-kamailio**, **xivo-acceptance**, **xivo-agid**, **xivo-confd**, **xivo-confgend**, **xivo-config**, **xivo-dao**, **xivo-db**, **xivo-lib-python**, **xivo-monitoring**, **xivo-sysconfd**, **xivo-upgrade**, **xivo-web-interface**, **xivocc-installer**, **xucmgt**, **xucserver**

**Desktop Assistant**

* `#4211 <https://projects.xivo.solutions/issues/4211>`_ - Desktop App - Handle right-clic menu (to paste or copy - for example in MR chat)

**Reporting**

* `#5173 <https://projects.xivo.solutions/issues/5173>`_ - SpagoBI - improve datatypes for XLS export in sample reports

**Switchboard**

* `#4461 <https://projects.xivo.solutions/issues/4461>`_ - [UC Switchboard] Make Switchboard/POPC possible in Desktop Applications

**Web Assistant**

* `#5283 <https://projects.xivo.solutions/issues/5283>`_ - Stop ringing on assistant when the call was answered on mobile

**WebRTC**

* `#5175 <https://projects.xivo.solutions/issues/5175>`_ - Information message you do not have an active voicemail when calling *98

**XUC Server**

* `#5267 <https://projects.xivo.solutions/issues/5267>`_ - When dialing from UC, we should not make ring Mobile APP
* `#5269 <https://projects.xivo.solutions/issues/5269>`_ - [C] - Cannot transfer because of ghost calls in xuc when fallback to trunk on another mds
* `#5344 <https://projects.xivo.solutions/issues/5344>`_ - xucserver - function isSipAttTransfer works only with chan_sip

**XiVO PBX**

* `#5060 <https://projects.xivo.solutions/issues/5060>`_ - Change webi mode host to mode bridge
* `#5232 <https://projects.xivo.solutions/issues/5232>`_ - Dotenv issue in xivo-confgend logs
* `#5286 <https://projects.xivo.solutions/issues/5286>`_ - We should a message in user configuration page if you have no labels
* `#5294 <https://projects.xivo.solutions/issues/5294>`_ - Remove dotenv requirements from xivo-lib-python and other projects
* `#5353 <https://projects.xivo.solutions/issues/5353>`_ - Labels - when more than 150 labels, labels selection component in user form is broken
* `#5354 <https://projects.xivo.solutions/issues/5354>`_ -  Mobile application: 2 registers on the Xivo side

**XiVOCC Infra**

* `#5284 <https://projects.xivo.solutions/issues/5284>`_ - Cannot make ring UC and mobile app at the same time

2022.06
=======

Consult the `2022.06 Roadmap <https://projects.xivo.solutions/versions/299>`_.

Components updated: **xivo-confd**, **xivo-config**, **xivo-dao**, **xivo-db**, **xivo-monitoring**, **xivo-web-interface**, **xivo-webi-nginx**, **xivocc-installer**

**XiVO PBX**

* `#4723 <https://projects.xivo.solutions/issues/4723>`_ - Change nginx mode host to mode bridge
* `#5198 <https://projects.xivo.solutions/issues/5198>`_ - Add option to wizard API to bypass network configuration
* `#5233 <https://projects.xivo.solutions/issues/5233>`_ - Add an option in wizard API to be able to run post-wizard scripts
* `#5242 <https://projects.xivo.solutions/issues/5242>`_ - Improve web-interface labels with filtering and better display for long lists
* `#5246 <https://projects.xivo.solutions/issues/5246>`_ - IVR access is broken
* `#5258 <https://projects.xivo.solutions/issues/5258>`_ - Labels page does not work with latest nginx