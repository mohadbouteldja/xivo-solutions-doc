.. _xivosolutions_release:

*************
Release Notes
*************

.. _jabbah_release:

Jabbah
======

Below is a list of *New Features* and *Behavior Changes* compared to the previous LTS version, Izar (2022.05).

New Features
------------
* New key ``run_scripts`` was added to wizard json. It switch if custom scripts are run after wizard.
  Default is ``False``.

Behavior Changes
----------------


Deprecations
------------

This release deprecates:

* `LTS Deneb (2019.12) <https://documentation.xivo.solutions/en/2019.12/>`_: This version is no longer supported.
  No bug fixes, no security update will be provided for this release.

Upgrade
-------

.. _upgrade_lts_manual_steps:

**Manual steps for LTS upgrade**

.. warning:: **Don't forget** to read carefully the specific steps to upgrade from another LTS version

.. toctree::
   :maxdepth: 1

   upgrade_from_five_to_polaris
   upgrade_from_polaris_to_aldebaran
   upgrade_from_aldebaran_to_borealis
   upgrade_from_borealis_to_callisto
   upgrade_from_callisto_to_deneb
   upgrade_from_deneb_to_electra
   upgrade_from_electra_to_freya
   upgrade_from_freya_to_gaia
   upgrade_from_gaia_to_helios
   upgrade_from_helios_to_izar
   upgrade_from_izar_to_jabbah


**Generic upgrade procedure**

Then, follow the generic upgrade procedures:

* :ref:`XiVO PBX upgrade procedure <upgrade>`
* :ref:`XiVO CC upgrade procedure <upgrade_cc>`
* :ref:`XDS upgrade procedure <upgrade_xds>`

Jabbah Bugfixes Versions
========================

Components version table
------------------------

Table listing the current version of the components.

+----------------------+----------------+
| Component            | current ver.   |
+======================+================+
| **XiVO**                              |
+----------------------+----------------+
| XiVO PBX             |                |
+----------------------+----------------+
| config_mgt           |                |
+----------------------+----------------+
| db                   |                |
+----------------------+----------------+
| outcall              |                |
+----------------------+----------------+
| db_replic            |                |
+----------------------+----------------+
| nginx                |                |
+----------------------+----------------+
| webi                 |                |
+----------------------+----------------+
| switchboard_reports  |                |
+----------------------+----------------+
| **XiVO CC**          |                |
+----------------------+----------------+
| elasticsearch        |                |
+----------------------+----------------+
| kibana               |                |
+----------------------+----------------+
| logstash             |                |
+----------------------+----------------+
| mattermost           |                |
+----------------------+----------------+
| nginx                |                |
+----------------------+----------------+
| pack-reporting       |                |
+----------------------+----------------+
| pgxivocc             |                |
+----------------------+----------------+
| recording-rsync      |                |
+----------------------+----------------+
| recording-server     |                |
+----------------------+----------------+
| spagobi              |                |
+----------------------+----------------+
| xivo-full-stats      |                |
+----------------------+----------------+
| xuc                  |                |
+----------------------+----------------+
| xucmgt               |                |
+----------------------+----------------+
| **Edge**             |                |
+----------------------+----------------+
| edge                 |                |
+----------------------+----------------+
| nginx                |                |
+----------------------+----------------+
| kamailio             |                |
+----------------------+----------------+
| coturn               |                |
+----------------------+----------------+
| **Meeting Rooms**    |                |
+----------------------+----------------+
| meetingroom          |                |
+----------------------+----------------+
| web-jitsi            |                |
+----------------------+----------------+
| jicofo-jitsi         |                |
+----------------------+----------------+
| prosody-jitsi        |                |
+----------------------+----------------+
| jvb-jitsi            |                |
+----------------------+----------------+
| jigasi-jitsi         |                |
+----------------------+----------------+
| **IVR**              |                |
+----------------------+----------------+
| ivr-editor           |                |
+----------------------+----------------+

Jabbah
-------

Jabbah Intermediate Versions
============================

.. toctree::
   :maxdepth: 2

   jabbah_iv
