.. highlightlang:: rest

.. _ccmanager:

*************************
Contact Center Management
*************************

Introduction
============


.. figure:: ccmanager.png
    :scale: 50%

CCmanager is a web application to manage and supervise a contact center, different menus are available from hamburger icon with following features:

Start the application : http://<xucmgt:port>/ccmanager

Global view
-----------

Queues and penalties with real time activity of each agent, possible options are

* Enable Compact view (remove queue statistics)
* Show/Hide agents that are not logged in
* Manage agents in queues:

   * Add/Remove agents from queues,
   * Drag&Drop agents from queue to queue,
   * Change agent penalties
   * Login/Logout, Pause/Unpause, Call [#f2]_ or Listen to [#f2]_ an agent


Group view
----------

Distribution of agents per queues


Queue view
----------

Activity per queue


Agent view
----------

Activity per agent, possible :ref:`ccmanager_actions_agent` are [#f1]_

* Login / Logout
* Pause / Available
* Listen [#f2]_
* Call [#f2]_

Callback view
-------------

List of callbacks. See :ref:`ccmanager_callbacks`.


Qualification view
------------------

To export calls qualification. See :ref:`ccmanager_qualifications`.


Authorizations and Access Control
=================================

Access to the application is restricted to authorized users(see :ref:`ccmanager-security`).


.. _ccmanager_actions_agent:

Agents actions
==============

As a supervisor you have some actions available on agent [#f1]_:

* Login / Logout
* Pause / Available
* Listen [#f2]_
* Call [#f2]_

.. _ccmanager_actions_agent-listen:

Listen to agent's conversation
------------------------------

While an agent is on call, as a supervisor you can listen to it's call.
To do this:

* use the *Listen* action on a agent,
* then you'll receive a call on your phone [#f2]_
* when answered you'll be listening to the agent conversation
* use DTMF to change listening mode:

   * ``4`` spy mode: the supervisor listens to the conversation
   * ``5`` whisper mode: as *spy mode* plus the supervisor can speak to the agent without the other participant to hear
   * ``6`` barge mode: as *spy mode* plus the supervisor can speak to both the agent and the other participant (e.g. the customer)

.. note:: By default when a supervisor spies on an agent the agent is only warned via the :ref:`CC Agent Call control <agent_call_control>` listen icon.

   *XiVO CC* can be configured to warn the agent via a beep and/or a lit key on its deskphone - see :ref:`ccmanager-warn-when-spied`.


Queue statistics
================

.. figure:: queue-stats.png
    :scale: 100%

This diagram shows some aggregation about statistics collected from queue activity

1. Percentage of calls answered before 15s
2. Percentage of calls abandoned after 15s
3. Number of available agents to take calls
4. Number of pending calls not answered yet


Queue details and configuration
===============================
By clicking on the name of the queue, a modal will appear.
Supervisors with no access to dissuasion will see some details about the queue.
Administrators and supervisors who have access to the dissuasion will see different tabs :

* One tab showing some information about the queue
* One tab to configure the dissuasion in case of an exceptional closing for this queue.

.. _ccmanager_activity_failed_dst:

Configuring the dissuasion for a queue
--------------------------------------
In the configuration tab, click on the title "Failed Destination".
A dropdown appears, showing the sound files and/or the queue that can be set up as a failed destination.
The failed destination can be changed from this dropdown.

.. figure:: ccmanager_configuring_dissuasion.png
    :scale: 80%

To change the failed destination options for this queue, see :ref:`agent_configuration-activity_failed_dst` section.

Editing Agent Configuration
===========================

.. figure:: agent-edit.png
    :scale: 50%

This interface allows a user to change queue assignement and the associated penalty. The queue table display the following columns

* "Number": The queue number
* "Name": The queue name
* "Penalty": The active penalty for the corresponding queue
* "default": The default penalty for the corresponding queue

The queue/active penalty couples can be saved as default configuration by clicking the "Set default" button, then "Save".
The queue/default penalty couples can be saved as active configuration by clicking the "Set current" button, then "Save".

.. note:: Removing an agent from a queue

    * Emptying the penalty textbox and saving will remove the queue from the active configuration for the agent.
    * Emptying the default textbox and saving will remove the queue from the default configuration for the agent.


Multiple Agent Selection
========================

From agent view you are able to add or remove more than one agent at the same time.

.. figure:: multipleagentselect.png
    :scale: 80%

Once the agent selection is done, click on the edit button to display the configuration window

.. figure:: agentsconfiguration.png
    :scale: 80%

Click on the plus button to add a queue for selection, click on the minus button to remove a queue to the selection.
Once queue to add or removed are choosen, click on save button to apply your configuration change.

Click on "Apply default configuration" to apply existing default configuration to all selected users and make it the active configuration. This action only affects users with an existing default configuration, agents whithout default configuration remain unchanged.


Agent Base Configuration
========================

From the agent view, after selecting one or more agents, you can create a base configuration by clicking on one of the menu item in the following drop down:

.. figure:: ccmanager-base-batch-dropdown.png
   :scale: 80%

* 'Create base configuration' will allow you to create a base configuration from scratch for all the selected agents.
* 'Create base configuration from active configuration' will allow you to create a base configuration using the selected agents active configuration. The queue membership and penalty populated will be built based on the merged membership of all the selected agents. In case of conflict, the lowest penalty will be used.

In both cases, you will be able to review your changes before applying them. The 'Create base configuration' popup is similar to the single agent edition popup:

.. figure:: ccmanager-base-batch-create.png
   :scale: 80%

The queue table display the following columns:

* "Number": The queue number
* "Name": The queue name
* "Penalty": The active penalty for the corresponding queue

Click on the plus button to add a queue for selection. Once your configuration is complete, click on save button to apply your configuration change.

.. _ccmanager_base_configuration_filter:

Applying Default Configuration
------------------------------

In order to re-apply or apply a default configuration, you may select agent whose base configuration is different from active configuration.

In the agent view, you will find a new column (Base config.) displaying if the base configuration is different from the active one:

.. figure:: ccmanager-base-filter.png
   :scale: 80%

The possible values for this field are:

* "n/a": The base configuration is not available for this agent
* "Ok" : The base configuration match the active configuration
* "Different": The base configuration **does not** match the active configuration

You can use this column to filter agent whose base configuration is different from the active one and then apply the default configuration by using the "Edit agent" option.

.. _ccmanager_queue_recording_activation:

Queue Recording
===============

Description
-----------

Once you setup queue recording in XiVO (see :ref:`queue_recording_configuration`), visual indicators are displayed next to queue name in **Global view**. Respectively following icons represents **recording mode** set on the queue.

.. figure:: ccmanager_queue_record_info.png

Furthermore shortcut action is displayed in the left menu to control the activation / deactivation of **all** recorded queues.
The switch will change its position in case the queue's recording status is activated / deactivated through XiVO Web Interface accordingly.

Global recording activation
---------------------------

The switch button will either activate all queues configured with **recording mode** set (*Recorded* or *Recorded on demand*), or stop the recording feature for all queues.

.. figure:: ccmanager_queue_recording.png
   :scale: 80%


.. note:: Action is applied only for next calls. Ongoing call recording is not started nor stopped when switch is triggered.

.. _ccmanager_thresholds:

Thresholds
==========

Additional Queue statistics columns
-----------------------------------

It is possible to add columns to reflect more performance indicators, such as **Percentage of calls answered/abandoned within XX seconds**.
See :ref:`statistics_configuration` to add such stats thresholds.

.. figure:: ccmanager_queue_thresholds.png
    :scale: 100%

Colors
-------

Color thresholds can be define for the waiting calls counter and the maximum waiting time counter

.. figure:: ccmanager_thresholds.png
    :scale: 80%

Once done, it is applied to the queue view and the global view

.. _ccmanager_callbacks:

Callbacks
=========

This view allows to manage callback request see :ref:`Managing Callbacks Using CCManager <callbacks_with_ccmanager>` for details.

.. _ccmanager_qualifications:

Exporting qualifications
========================

This view allows to export calls qualification see :ref:`Call Qualifications <call_qualification>`.

To export the qualification answers, open **Qualifification View** page.

.. figure:: ccmanager_qualifications_export.png
   :scale: 85%

Select the date from, date to and queue. Then click to Download button.
This will open new page with CSV file to download.

   .. warning::

      When exporting data from the same day, select **date to** to be +1 day of **date from**.


.. rubric:: Footnotes
.. [#f1] Available actions depend on the state of the agent
.. [#f2] Only supervisors which have their own line can listen to or call agents, not supported for mobile supervisors, a line has to be affected to supervisors in xivo
