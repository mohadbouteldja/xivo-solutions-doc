.. _xuc_api:

*******************************
Unified Communication Framework
*******************************

This framework is mainly provided by the xuc server. It provides

* Javascript API
* Rest Web services
* Sample application
* Real Time Statistics

.. toctree::
   :maxdepth: 3

   api/wsapi
   api/rest
   stats/statistics
   technical/structure
   

