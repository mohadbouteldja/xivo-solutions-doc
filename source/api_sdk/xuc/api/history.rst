.. _history_api:

***********
History API
***********

This API to retrieve any kind of call history (user, agent, queue, customer...)

A sample of implementation is available in `app/assets/javascripts/pages/sampleHistory.js`
and `app/views/sample/sampleHistory.scala.html`

History Methods
===============

Cti.getUserCallHistory(size)
----------------------------

Get the call history of the logged in user, limited to the last `size` calls.
If size is `null`, the last 10 days of history will be returned.

Cti.getUserCallHistoryByDays(days)
----------------------------------

Get the call history of the logged in user, limited to the last `days` days.

Cti.getAgentCallHistory(size)
-----------------------------

Get the call history of the logged in agent, limited to the last `size` calls.

Cti.getQueueCallHistory(queue, size)
------------------------------------

Get a call history for a queue or a set of queues. You may pass part of a queue name (not display name).

i.e. pass bl if you want to match queue name blue, black and blow

History Events
==============

Associated Handler CALLHISTORY
------------------------------

Received when calling the above methods `Cti.getAgentCallHistory(size)`_ or `Cti.getUserCallHistory(size)`_ or `Cti.getUserCallHistoryByDays(days)`_.

* CALLHISTORY : "CallHistory"

::

   {
        "start":"2014-01-01 08:00:00",
        "duration":"00:21:35",
        "srcNum":"0115878",
        "dstNum":"2547892",
        "status":"answered"
   }

For queue calls status can be :

    * full - full queue
    * closed - closed queue
    * joinempty - call arrived on empty queue
    * leaveempty - exit when queue becomes empty
    * divert_ca_ratio -call redirected because the ratio waiting calls/agents was exceeded
    * divert_waittime - call redirected because estimated waiting time was exceeded;
    * answered - call answered
    * abandoned - call abandoned
    * timeout - maximum waiting time exceeded

For other calls

    * emitted
    * missed
    * ongoing
