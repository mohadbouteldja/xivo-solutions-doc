.. _javascriptapi:

***********
Generic API
***********

Generic CTI Methods
===================

Cti.getList(objectType)
-----------------------
Request a list of configuration objects, objectType can be :

* queue
* agent
* queuemember

Triggers handlers QUEUELIST, AGENTLIST, QUEUEMEMBERLIST.
Subscribes to configuration modification changes, handlers QUEUECONFIG, AGENTCONFIG, QUEUEMEMBER can also be called

Generic CTI Events
==================

Error
-----
* Cti.MessageType.ERROR

Is triggered whenever service answered abnormally to the request.

LoggedOn
--------
* Cti.MessageType.LOGGEDON

Is triggered once a user is properly authenticated and logged in.

Link Status Update
------------------
* Cti.MessageType.LINKSTATUSUPDATE

Is triggered when connection to server is lost.
