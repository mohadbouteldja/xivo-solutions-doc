******
Wizard
******

After any clean installation of XiVO, it must be initialized through a program called wizard.
Wizard is usually passed through the web interface, but there is another way - through a REST API.

You can pass the wizard with a single request to the API, by providing a specific JSON body as argument.
You can use the swagger found on XIVO_IP/api > xivo_confd > wizard to send the POST request.
The body to fill has this form :

.. figure:: images/wizard_api_json_body.png


Some key's meaning will be detailed below.

run_scripts
***********

Value: `true | false`, default is 'false'

If set to `true`, after the wizard is finished, all executable scripts in directory `/etc/xivo/wizard.d` are going to be run.

Those scripts must be runnable by xivo-confd (file has permission set to 'x' for `www-data`) plus any necessary right to perform their additionnal actions.

The scripts are run in ascii order (0-9, A-Z, a-z). In case of unsuccessful run,
an exception is raised and all non-finished scripts are stopped. You can then find the failing script's output in `xivo-confd` log.
Another exception is raised if the directory `/etc/xivo/wizard.d` doesn't exist.
