.. _meetingrooms_config:

***************************
Meeting Rooms Configuration
***************************

.. contents:: :local:

Meeting Rooms configuration
===========================

Create default env file
-----------------------

#. **Launch the following commands** that will *generate* the :file:`.env` file:

   .. warning:: Please **Copy** the **whole** code block below and **Paste** it in a terminal. It will (re)generate the :file:`.env` file.
      Do not copy-paste only the :file:`.env` content because it contains specific *here-document* syntax.

   .. code-block:: bash

    function generatePassword() {
        openssl rand -hex 16
    }

    JICOFO_AUTH_PASSWORD=$(generatePassword)
    JVB_AUTH_PASSWORD=$(generatePassword)
    JIGASI_XMPP_PASSWORD=$(generatePassword)
    JIGASI_SIP_PASSWORD=$(generatePassword)

    cat > /etc/docker/meetingrooms/.env << EOF
    # XiVO vars
    XIVOCC_TAG=2022.05
    XIVOCC_DIST=latest
    XUC_HOST=<FILL IN>
    MEETING_ROOMS_HOST_IP=<FILL IN>
    XIVO_HOST=<FILL IN>
    #
    # XiVO Edge vars
    #
    TURN_CREDENTIALS=<FILL IN>
    TURN_HOST=<FILL IN>
    TURN_PORT=3478
    TURNS_HOST=\${TURN_HOST}
    TURNS_PORT=3478
    #
    # JWT Configuration
    ENABLE_AUTH=1
    ENABLE_GUESTS=0
    AUTH_TYPE=jwt
    JWT_APP_ID=xivo
    JWT_APP_SECRET=<FILL IN>
    #
    # Security
    #
    JICOFO_AUTH_PASSWORD=${JICOFO_AUTH_PASSWORD}
    JVB_AUTH_PASSWORD=${JVB_AUTH_PASSWORD}
    JIGASI_XMPP_PASSWORD=${JIGASI_XMPP_PASSWORD}
    #
    # Basic config
    #
    CONFIG=/etc/docker/meetingrooms/jitsi/
    DOCKER_HOST_ADDRESS=\${MEETING_ROOMS_HOST_IP}
    HTTP_PORT=80
    HTTPS_PORT=443
    TZ=Europe/Paris
    PUBLIC_URL=https://\${XUC_HOST}/video
    ENABLE_CLOSE_PAGE=true
    ENABLE_SIMULCAST=false
    #
    # Basic Jigasi configuration options
    #
    JIGASI_SIP_URI=xivo-jitsi@\${XIVO_HOST}
    JIGASI_SIP_PASSWORD=${JIGASI_SIP_PASSWORD}
    JIGASI_SIP_SERVER=\${XIVO_HOST}
    JIGASI_SIP_PORT=5060
    JIGASI_SIP_TRANSPORT=UDP
    #
    #  Basic Video configuration option
    #
    DESKTOP_SHARING_FRAMERATE_MIN=5
    DESKTOP_SHARING_FRAMERATE_MAX=30
    #
    # Advanced configuration options (you generally don't need to change these)
    #
    XMPP_DOMAIN=meet.jitsi
    XMPP_SERVER=xmpp.meet.jitsi
    XMPP_BOSH_URL_BASE=http://xmpp.meet.jitsi:5280
    XMPP_AUTH_DOMAIN=auth.meet.jitsi
    XMPP_MUC_DOMAIN=muc.meet.jitsi
    XMPP_MODULES=muc_size
    XMPP_INTERNAL_MUC_DOMAIN=internal-muc.meet.jitsi
    XMPP_GUEST_DOMAIN=guest.meet.jitsi
    JVB_BREWERY_MUC=jvbbrewery
    JVB_AUTH_USER=jvb
    JVB_STUN_SERVERS=meet-jit-si-turnrelay.jitsi.net:443
    JVB_PORT=10000
    JVB_TCP_HARVESTER_DISABLED=true
    JVB_TCP_PORT=4443
    JVB_TCP_MAPPED_PORT=4443
    JICOFO_AUTH_USER=focus
    JIGASI_XMPP_USER=jigasi
    JIGASI_BREWERY_MUC=jigasibrewery
    JIGASI_PORT_MIN=20000
    JIGASI_PORT_MAX=20050
    XMPP_RECORDER_DOMAIN=recorder.meet.jitsi
    JIBRI_RECORDER_USER=recorder
    JIBRI_XMPP_USER=jibri
    JIBRI_BREWERY_MUC=jibribrewery
    JIBRI_PENDING_TIMEOUT=90
    RESTART_POLICY=unless-stopped
    ENABLE_P2P=false
    EOF

#. Then open the :file:`/etc/docker/meetingrooms/.env` and fill in:

  * ``XUC_HOST`` var with the same content as the XUC_HOST var in your **XiVO CC**
    configuration
  * ``MEETING_ROOMS_HOST_IP`` var with the meeting rooms server IP address.
    It must be the meeting rooms server IP address accessible for your LAN client.
    It must be set otherwise nothing will work correctly.
  * ``XIVO_HOST`` with the VoIP IP of the XiVO PBX
    It will be the IP towards which the Jitsi SIP gateway will register
    its SIP peer.
  * ``JWT_APP_SECRET`` with the same content as the CONFIGMGT_AUTH_SECRET var defined in your **XiVO**'s /etc/docker/xivo/custom.env file
  * Fill in also parameters linked to :ref:`XiVO Edge <edge_install>` (which is a prerequisite to use Meeting Room Server - see :ref:`meetingrooms_install_requirement`):

    * ``TURN_HOST`` var with the Edge STUN/TURN server FQDN
      (i.e. which must be a FQDN corresponding to the certificate configured on the Edge STUN/TURN server).
    * ``TURN_CREDENTIALS`` var with the value of the ``TURN_SECRET`` configure in the Edge TURN Server (the secret generated during :ref:`edge_config_turnsecret`).


Start the services
------------------

Start the services:

.. code-block:: bash

  docker login -u xivoxc
  (use the token provided by the XiVO team)

  meetingrooms-dcomp pull && docker logout
  meetingrooms-dcomp up -d

Edge Configuration
==================

.. note:: These steps are to be done on the XiVO CC.

On the Edge server you must add the Meetingroom server IP address in the ``TURN_ALLOWED_PEERS``.

Please refer to :ref:`edge_config_turnrelaypermission` section.


XIVO CC Configuration
=====================

.. note:: These steps are to be done on the XiVO CC.

   If you are using the :ref:`xivouc_addon` mode
   you **must follow** the sub-section below: :ref:`meetingroom_config_xivoucaddon`

On XiVO CC, to be able to use the video service you **must** configure the
``NGINX_JITSI_HOST`` variable.

#. Add the ``NGINX_JITSI_HOST`` value in the :file:`/etc/docker/compose/custom.env` file:

   .. code-block:: ini

    NGINX_JITSI_HOST=<IP Address of the Meeting Rooms server>

#. Relaunch the services
  
  .. code-block:: bash
  
   xivocc-dcomp up -d
  

.. _meetingroom_config_xivoucaddon:

XiVO UC add-on Configuration
----------------------------

.. important:: This section applies only if you are in the :ref:`xivouc_addon` mode.

With XiVO UC add-on, to be able to use the video service you **must** configure the
``NGINX_JITSI_HOST`` variable.

#. Add the ``NGINX_JITSI_HOST`` value in the :file:`/etc/docker/xivo/custom.env` file:

   .. code-block:: ini

    NGINX_JITSI_HOST=<IP Address of the Meeting Rooms server>

#. Relaunch the services
  
  .. code-block:: bash
  
   xivo-dcomp up -d


XiVO Configuration
==================

.. note:: These steps are to be done on the XiVO

   If you are using the :ref:`xivouc_addon` mode
   you must also follow the section :ref:`meetingroom_config_xivoucaddon`


Configure Jitsi properties in your custom env
---------------------------------------------

#. Edit :file:`/etc/docker/xivo/custom.env` file :

   .. code-block:: ini

    MEETINGROOM_AUTH_DOMAIN=<The domain serving the service, i.e. "avencall.com">
    MEETINGROOM_AUTH_APP_ID=<Value of the JWT_APP_ID defined in your jitsi .env file>

Configure the Trunk to Jitsi
----------------------------

On XiVO, go on page :menuselection:`Services --> IPBX --> Trunk management -->
SIP Protocol`, and create a SIP trunk with:

* Tab *General*:

  * :guilabel:`Name`: xivo-jitsi
  * :guilabel:`Username`: xivo-jitsi
  * :guilabel:`Password`: put the value of the ``JIGASI_SIP_PASSWORD``
    variable in the :file:`/etc/docker/meetingrooms/.env`
  * :guilabel:`Connection type`: Peer
  * :guilabel:`IP addressing type`: Dynamic
  * :guilabel:`Context`: default
  * :guilabel:`Media server`: MDS Main
  * :guilabel:`NAT`: Yes (force rport + comedia)

* Tab *Advanced*:

  * :guilabel:`Redirect media streams`: No


Then restart the jigasi docker on Meeting Room server::

    meetingrooms-dcomp restart jigasi
