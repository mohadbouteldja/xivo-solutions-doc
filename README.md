Xivo-solutions-doc
==================

This is the documentation project of the XiVO Solutions. It is available at http://xivosolutions.readthedocs.io/.


Requirements
------------

* pip (sudo apt install python-pip) => Deprecated

To install python2.7 and pip after the deprecation :

Go to the official Python website and download the gzipped source tarball
https://www.python.org/downloads/release/python-2716/

Extract the folder from the tarball with the command
```console
tar xvzf Python-2.7.16.tgz
```

Cd into the extracted folder and run :

```console
chmod +x ./configure
./configure
make
sudo make install
```

It will configure and then install Python2.7 on your computer.

Then, go to the official pip website and follow the steps to install pip for
Python 2.7 : 

https://pip.pypa.io/en/stable/installing/
```console
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
python get-pip.py
```
After the installation, you will need to add /home/YOUR_USERNAME/.local/bin to your PATH env variable.

Then, you should be able to install virtualenv with pip 2.7 and run make clean html. 

Dependencies
------------

* Sphinx (package python-sphinx on Debian)
* Python dev (package python-dev on Debian)
* sphinx-git modified (for git changelog). To install it:

  pushd /tmp && sudo pip install git+https://gitlab.com/xivo.solutions/sphinx-git.git@tagtitles && popd

Build
-----

   make clean html


PDF version
-----------

You will need a LATEX compilation suite. On Debian, you can use the following
packages :

$ apt-get install texlive-latex-base texlive-latex-recommended
texlive-latex-extra texlive-fonts-recommended


Troubleshooting
---------------

If the build fails with error `ImportError: cannot import name _remove_dead_weakref`, run:

    rm -rf envs
    pip install virtualenv
